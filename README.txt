Coding Test (fictitious)


***
NOTE: PLEASE DO NOT USE THIRD PARTY LIBRARIES. However, feel free to state which third party libraries you might have used.
***


Attached you’ll find the code for a simple mobile app to browse cakes. Unfortunately, the developer who wrote this code is no longer employed by the company as we had some concerns over his ability. The project compiles but the app crashes as soon as it runs.

The app loads a JSON feed containing a repeated list of cakes, with title, image and description from a URL, then displays the contents of the feed in a scrollable list.

We would like you to fix the crash bug, ensure the functionality of the app works as expected (all images display correctly on the table, all text is readable) and performs smoothly.  Use of 3rd party libraries are prohibited for this project due to its sensitive nature.

Additionally, feel free to refactor and optimise the code where appropriate. Ideally, what we’d be looking for is:

* Simple fixes for crash bug(s)
* Application to support rotation
* Safe and efficient loading of images
* Removal of any redundant code
* Refactoring of code to best practices

This is your opportunity to show us how you think and Android app should be architected, please make any changes you feel would benefit the app.

The test should take around 2 hours, certainly no longer than 3 hours. Good luck!

*** SOLUTION ***

DONE
--------

Put the async loading classes we need into their own class files

An HTTP/URL utility op, so give it an HTTPUtils class

Put the configured JSON_URL into build config
    Could use alternate endpoints in test config

Fix layout.
    Fix broken right of relative to position text after images.
    Changed a bit so the title is bigger. Designer, what look do you want ?
    Not sure why we want it 256dp wide instead of match_parent,
    but it's a fragment so there *might* be a reason ?

Refactored the fragment class out of the activity
	   This is so trivial in Studio, which is nice
	   Added the viewholder pattern to recycle views properly in the list

Build a separate Cake model class

Rotation seems to work off the bat, or at least given what I've done
	 New activity/fragment is created on rotation, and they just work

TODO
--------

Possible common subclass LoadDataTask and LoadImageTask
	 But most likely it's overkill

Use GZIP headers for http
    Will compress nicely if the server can support gzip

Use a cache of loaded bitmap images, as per suggestion in code
    either cache from Guava
    or completely using Picasso, with its own cacheing
    Try not to roll our own cacheing

Use a 3rd party JSON library
    GSON takes JSON object straight into a model object
    	 No hand-parsing
	 Could use retrofit or volley to configure back end communication and do away with all the hand rolled HTTP stuff
	 Could argue to use Rx or one of many other concurrency "solutions" on offer, rather than AsyncTask

For anything with more content, I'd use MVP
    Add a CakeListPresenter responsible for fetching the cakes and putting the values into the CakeListFragment.
