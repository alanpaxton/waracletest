package com.waracle.androidtest;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Model class for cakes.
 *
 * Created by alan on 25/08/2016.
 */
public class Cake {

    private final static String TAG = "Cake";

    public String title;
    public String desc;
    public String image;

    public static List<Cake> cakesFromJSON(JSONArray array) {
        List<Cake> cakes = new ArrayList<>(array.length());

        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                Cake cake = new Cake();
                cake.title = object.optString("title");
                cake.desc = object.optString("desc", "");
                cake.image = object.optString("image");
                if (!TextUtils.isEmpty(cake.title)) {
                    cakes.add(cake);
                }
            } catch (JSONException e) {
                Log.e(TAG, "Object at array index " + i + " is bad ", e);
            }
        }
        return cakes;
    }
}
