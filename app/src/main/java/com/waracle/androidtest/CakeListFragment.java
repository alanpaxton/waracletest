package com.waracle.androidtest;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 * Fragment is responsible for loading in some JSON and
 * then displaying a list of cakes with images.
 * Fix any crashes
 * Improve any performance issues
 * Use good coding practices to make code more secure
 */
public class CakeListFragment extends ListFragment {

    private static String JSON_URL = BuildConfig.JSON_HOST + BuildConfig.JSON_PATH;

    private static final String TAG = CakeListFragment.class.getSimpleName();

    private ListView mListView;
    private CakeListAdapter mAdapter;

    public CakeListFragment() { /**/ }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mListView = (ListView) rootView.findViewById(android.R.id.list);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Create and set the list adapter.
        mAdapter = new CakeListAdapter();
        mListView.setAdapter(mAdapter);

        // Load data from net, using an async task to do the loading.
        try {
            new LoadDataTask() {
                protected void onPostExecute(JSONArray array) {
                    if (array != null) {
                        List<Cake> cakes = Cake.cakesFromJSON(array);
                        mAdapter.setItems(cakes);
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }.execute(new URL(JSON_URL));
        } catch (MalformedURLException e) {
            Log.e(TAG, "Configured URL was not valid " + JSON_URL);
        }
    }

    /**
     * Hold the member views of a list item without reloading via id
     */
    private static class ViewHolder {
        public TextView title;
        public TextView desc;
        public ImageView image;
    }

    private class CakeListAdapter extends BaseAdapter {

        // Can you think of a better way to represent these items???
        private List<Cake> mItems;
        private ImageLoader mImageLoader;

        public CakeListAdapter() {
            this(Collections.EMPTY_LIST);
        }

        public CakeListAdapter(List<Cake> items) {
            mItems = items;
            mImageLoader = new ImageLoader();
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());

            View itemView;
            ViewHolder viewHolder;

            if (convertView == null) {
                itemView = inflater.inflate(R.layout.list_item_layout, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.title = (TextView) itemView.findViewById(R.id.title);
                viewHolder.desc = (TextView) itemView.findViewById(R.id.desc);
                viewHolder.image = (ImageView) itemView.findViewById(R.id.image);

                itemView.setTag(viewHolder);
            } else {
                itemView = convertView;
                viewHolder = (ViewHolder)itemView.getTag();
            }
            if (viewHolder != null) {
                Cake cake = (Cake)getItem(position);
                viewHolder.title.setText(cake.title);
                viewHolder.desc.setText(cake.desc);
                mImageLoader.load(cake.image, viewHolder.image);
            }

            return itemView;
        }

        public void setItems(List<Cake> items) {
            mItems = items;
        }
    }
}
