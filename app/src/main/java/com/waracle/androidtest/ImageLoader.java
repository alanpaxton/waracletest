package com.waracle.androidtest;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidParameterException;

/**
 * Created by Riad on 20/05/2015.
 */
public class ImageLoader {

    private static final String TAG = ImageLoader.class.getSimpleName();

    public ImageLoader() { /**/ }

    /**
     * Simple function for loading a bitmap image from the web
     *
     * @param url       image url
     * @param imageView view to set image too.
     */
    public void load(String url, final ImageView imageView) {
        if (TextUtils.isEmpty(url)) {
            throw new InvalidParameterException("URL is empty!");
        }

        // Can you think of a way to improve loading of bitmaps
        // that have already been loaded previously??
        // These could be cached. If we used a 3rd party libary like Picasso, that would get done for us.
        // Or we could use something like Guava and use its cacheing behind our own image library.

        // Load data from net, using an async task to do the loading.
        imageView.setVisibility(View.INVISIBLE);
        try {
            new LoadImageTask() {
                protected void onPostExecute(byte[] image) {
                    if (image.length > 0) {
                        imageView.setImageBitmap(convertToBitmap(image));
                        imageView.setVisibility(View.VISIBLE);
                    }
                }
            }.execute(new URL(url));
        } catch (MalformedURLException e) {
            Log.e(TAG, "Image URL was not valid " + url);
        }
    }

    private static Bitmap convertToBitmap(byte[] data) {
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }
}
