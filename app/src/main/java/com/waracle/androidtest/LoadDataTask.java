package com.waracle.androidtest;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by alan on 25/08/2016.
 */
public class LoadDataTask extends AsyncTask<URL, Void, JSONArray> {

    private final static String TAG = "LoadDataTask";

    @Override
    protected JSONArray doInBackground(URL... urls) {

        if (urls.length == 1) {
            try {
                return loadData(urls[0]);
            } catch (IOException ioe) {
                Log.e(TAG, "Unable to load data", ioe);
            } catch (JSONException je) {
                Log.e(TAG, "Unable to load data", je);
            }
        }
        return null;
    }

    private JSONArray loadData(URL url) throws IOException, JSONException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            // Can you think of a way to improve the performance of loading data
            // using HTTP headers???
            // Could try requesting GZIP

            // Also, Do you trust any utils thrown your way????

            byte[] bytes = StreamUtils.readUnknownFully(in);

            // Read in charset of HTTP content.
            String charset = HTTPUtils.parseCharset(urlConnection.getRequestProperty("Content-Type"));

            // Convert byte array to appropriate encoded string.
            String jsonText = new String(bytes, charset);

            // Read string as JSON.
            return new JSONArray(jsonText);
        } finally {
            urlConnection.disconnect();
        }
    }
}
