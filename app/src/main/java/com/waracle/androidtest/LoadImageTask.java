package com.waracle.androidtest;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by alan on 25/08/2016.
 */
public class LoadImageTask extends AsyncTask<URL, Void, byte[]> {


    private final static String TAG = "LoadDataTask";

    @Override
    protected byte[] doInBackground(URL... urls) {

        if (urls.length == 1) {
            try {
                return loadData(urls[0]);
            } catch (IOException ioe) {
                Log.e(TAG, "Unable to load image", ioe);
            }
        }
        return null;
    }

    private byte[] loadData(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream inputStream = null;
        try {
            try {
                // Read data from workstation
                inputStream = connection.getInputStream();
            } catch (IOException e) {
                // Read the error from the workstation
                inputStream = connection.getErrorStream();
            }

            // Can you think of a way to make the entire
            // HTTP more efficient using HTTP headers??
            // GZIP header

            return StreamUtils.readUnknownFully(inputStream);
        } finally {
            // Close the input stream if it exists.
            StreamUtils.close(inputStream);

            // Disconnect the connection
            connection.disconnect();
        }
    }
}
